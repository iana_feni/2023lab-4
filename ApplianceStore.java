import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		//Initialize an array of appliances
		AirFryer [] airFry = new AirFryer[4];
		
		//Input fields for each appliance
		for(int i=0; i < airFry.length; i++)
		{
		 System.out.println("This is appliance number "+i);
         System.out.println("Enter the max degrees it can cook at, the max time it can cook for and it<s color:");
		 airFry[i] = new AirFryer(scan.nextInt(),scan.nextInt(),scan.next());
		
		}
		//Print 3 fields of the last applience
		System.out.println("Max degrees: "+airFry[3].getDegrees()+"  Max time: "+ airFry[3].getCookingTime()+" Color: "+airFry[3].getColor());
		
		//Print 2 methods
		airFry[0].maxCookingTime();
		airFry[0].maxDegrees();
		
		//Call the instance method on the second appliance
        airFry[1].colorOfAirFryer(airFry[1].getColor());		
		
		//Before
		System.out.println("The color of the last appliance before the update: "+airFry[3].getColor());
		System.out.println("The max cooking time of the last appliance before the update: "+airFry[3].getCookingTime());
		
		//Update the values of the lat appliance
		System.out.println("Enter the color of the last appliance:");
		airFry[3].setColor(scan.next());
		System.out.println("Enter the max cooking time of the last appliance:");
		airFry[3].setCookingTime(scan.nextInt());
		
		//After
	    System.out.println("The color of the last appliance after the update: "+airFry[3].getColor());
		System.out.println("The max cooking time of the last appliance after the update: "+airFry[3].getCookingTime());
	}
}