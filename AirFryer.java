public class AirFryer
{
	private int degrees;
	private int cookingTime;
	private String color;

    public void maxCookingTime(){
	  System.out.println("What's the maximun cooking time on your AirFryer?");
	  System.out.println(cookingTime+" min");
	}

	public void maxDegrees(){
	  System.out.println("What's the maximun degress your AirFryer can cook at?");
	  System.out.println(degrees+" degrees");
	}
	//Lab-4
	public void colorOfAirFryer(String colors){
		if(validation(colors))
		{
		this.color = colors;
		System.out.println("Good Entry");
		}
		else
		{
			System.out.println("Invalid Entry");
		}
	}

	//Helper method
	    private boolean validation(String colors){
	    boolean isValid = (colors.equals("white") || colors.equals("black") || colors.equals("grey"));
		return isValid;

	}
	//Get methods 
	public String getColor()
	{ 
	return this.color; 
	}
	
	public int getCookingTime()
	{ 
	return this.cookingTime; 
	}
	
	public int getDegrees()
	{ 
	return this.degrees; 
	}
  
    //Set methods
	public void setColor(String colors) 
	{ 
	    this.color=colors;
	}
	public void setCookingTime(int time) 
	{ 
	    this.cookingTime = time;
	}

	
	
	//Constructor
	public AirFryer(int degrees, int time, String color){
		this.color=color;
		this.degrees= degrees;
		this.cookingTime= time;
	}


}